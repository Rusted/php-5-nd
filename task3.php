<html>
<head>
</head>
<body>
<p>
<?php
ini_set('display_errors', '1');
const NORMAL_TEMPERATURE_MAX = 37;
const NORMAL_TEMPERATURE_MIN = 36.4;
if (empty($_GET['temperatures']) || empty($_GET['hours'])) {
    echo 'Įveskite temperatūras ir valandas';
} else {
    $temperatures = explode(',', $_GET['temperatures']);
    $hours = explode(',', $_GET['hours']);
    if (count($temperatures) !== count($hours)) {
        echo 'Įveskite vienodą skaičių įrašų';
    } else {
        $highestTemperature = max($temperatures);
        $normalTemperatures = [];
        $closeToHighestTemperatures = [];
        $highestTemperatures = [];

        foreach ($temperatures as $key => $temperature) {
            if ($temperature <= NORMAL_TEMPERATURE_MAX && $temperature >= NORMAL_TEMPERATURE_MIN) {
                $normalTemperatures[$hours[$key]] = $temperature;
            } elseif ($temperature < $highestTemperature && ($temperature >= $highestTemperature - 0.5)) {
                $closeToHighestTemperatures[$hours[$key]] = $temperature;
            } elseif ($temperature == $highestTemperature) {
                $highestTemperatures[$hours[$key]] = $temperature;
            }
        }

        ksort($normalTemperatures);
        ksort($closeToHighestTemperatures);
        ksort($highestTemperatures);

        if (!empty($normalTemperatures)) {
            $message = "Normali temperatūra buvo: ";
            foreach ($normalTemperatures as $hour => $temperature) {
                $message .= "$hour valandą: $temperature, ";
            }

            $message = substr($message, 0, -2);
            echo "$message.<br>";
        }

        if (!empty($closeToHighestTemperatures)) {
            $message = "Artima aukščiausiai temperatūra buvo: ";
            foreach ($closeToHighestTemperatures as $hour => $temperature) {
                $message .= "$hour valandą: $temperature, ";
            }

            $message = substr($message, 0, -2);
            echo "$message.<br>";
        }

        if (!empty($highestTemperatures)) {
            $message = "Aukščiausia temperatūra buvo: ";
            foreach ($highestTemperatures as $hour => $temperature) {
                $message .= "$hour valandą: $temperature, ";
            }

            $message = substr($message, 0, -2);
            echo "$message.<br>";
        }
    }
}
?>

</p>
    <form method="GET">
        <label>Hours:</label>
        <input name="hours" type="text" value="<?php echo isset($_GET['hours']) ? $_GET['hours'] : '' ?>" />
        <label>Temperatures:</label>
        <input name="temperatures" type="text" value="<?php echo isset($_GET['temperatures']) ? $_GET['temperatures'] : '' ?>" />
        <input type="submit" value="Siųsti" />
    </form>
</body>

</html>