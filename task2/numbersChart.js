function initNumbersChart(chartId, labels, numbers, filteredNumbers) {
    var ctx = document.getElementById(chartId).getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: "Numbers",
                borderColor: 'rgb(255, 99, 132)',
                data: numbers,
            }, {
                label: "Filtered numbers",
                borderColor: 'rgb(0, 99, 132)',
                data: filteredNumbers,
            }],
        },
        fill: false,
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZro: true
                    }
                }]
            }
        }
    });
}