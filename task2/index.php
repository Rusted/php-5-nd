<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body>
    <p>
<?php
$numbers = explode(',', $_GET['numbers']);
$filteredNumbers = [];
if (count($numbers) === 1) {
    $filteredNumbers = $numbers;
} else {
    foreach ($numbers as $key => $number) {
        if ($key === 0) { // pirmas skaičius
            $filteredNumbers[0] = round(($number + $numbers[1]) / 2);
        } elseif ($key === count($numbers) - 1) { // paskutinis skaičius
            $filteredNumbers[$key] = round(($number + $numbers[$key - 1]) / 2);
        } else {
            $filteredNumbers[$key] = round(($number + $numbers[$key - 1] + $numbers[$key + 1]) / 3);
        }
    }
}

?>
    </p>
    <form method="GET">
        <input name="numbers" type="text" />
        <input type="submit" value="Siųsti" />
    </form>

    <table border="1">
        <tr>
            <th>Skaičius</th>
            <th>Skaičius po filtravimo</th>
        </tr>
        <?php foreach ($numbers as $key => $number) {?>
            <tr>
            <td><?php echo $number; ?></td>
            <td><?php echo $filteredNumbers[$key] ?></td>
            </tr>
        <?php }?>
    </table>
    <canvas id="myChart"></canvas>
    <script src="numbersChart.js"></script>
    <script>
        var labels = [<?php echo implode(',', array_keys($numbers)); ?>];
        var numbers = [<?php echo implode(',', $numbers); ?>];
        var filteredNumbers = [<?php echo implode(', ', $filteredNumbers); ?>];
        initNumbersChart("myChart", labels, numbers, filteredNumbers);
    </script>
</body>

</html>